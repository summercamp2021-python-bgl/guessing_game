import random

# TODO:
# generate random number
# get user's guess
# compare results


try:
    # get range for prng from user
    range_min = int(input("Please choose your range \n\tMin Number >> "))
    range_max = int(input("\tMax Number >> "))

    # get max guesses
    max_guesses = input("Please select the max number of guesses (0 = infinite) >> ")
    pass
except ValueError as err:
    print("Please enter a number!")
    exit(1)
    pass

max_guesses = int(0 if max_guesses == "" else int(max_guesses))

running = True

stats_games = 0
stats_guesses = 0

while running:
    stats_games+=1
    guesses_this_game=0

    # get a random number
    guessable = random.randint(range_min, range_max)

    while True:
        #increment guesses
        stats_guesses+=1
        guesses_this_game+=1
        
        #check max guesses
        if(max_guesses and guesses_this_game > max_guesses):
            print("[result] max guesses reached")
            break
        
        # let user choose their number
        try:
            answer = int(input("Please guess your number >> "))
            pass
        except ValueError as err:
            print("[result] Please enter a number!")
            stats_guesses -= 1
            guesses_this_game -= 1
            continue

        # compare numbers
        if answer < guessable:     # if bigger, give hint
            print("[result] too small")
            pass
        elif answer > guessable:   # if smaller, give hint
            print("[result] too big")
            pass
        else:                      # if equal, exit loop
            print("[result] you guessed it!")
            break

        pass
    
    running = input("Want to try again? [Yn] >> ").upper() == "Y"
    pass

if input("Want to display your stats? [Yn] >> ").upper() == "Y":
    print(f"Stats:\n\t\
Games: {stats_games}\n\t\
Total Guesses: {stats_guesses}\n\t\
Average Guesses: {stats_guesses / stats_games}")
    pass
